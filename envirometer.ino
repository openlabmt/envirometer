// Environment meter: water conductibility, temperature and pH measure
// https://gitlab.com/openlabmt/envirometer
// Conductibility sensor v5 (30/05/2015) of Paolo Bonelli
// TODO: project description
// Use Arduino Mega

#include <Wire.h>
// Get the LCD I2C Library here: 
// https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
// Move any other LCD libraries to another folder or delete them
// See Library "Docs" folder for possible commands etc.
#include <LiquidCrystal_I2C.h>
//#include <SoftwareSerial.h>
#include "RTClib.h";

// DS18B20
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SD.h>
#include <SPI.h> // Used by SD library

RTC_DS1307 RTC;

// set the LCD address to 0x27 for a 20 chars 4 line display
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

const byte ITR_TEMPER_PIN = 2;
const byte ITR_PH_PIN = 3;
const byte ITR_CONDUCT_PIN = 19;

const byte DS18B20_PIN = 7;
const byte BUZZER_PIN = 9;
const byte SS_PIN_A = 53; // Arduino Mega
const int DELAY_BTN_PRESSED = 100;
// Conductivity sensor
const byte CONDUCT_P1_PIN = 10;
const byte CONDUCT_P2_PIN = A0;
const byte CONDUCT_P3_PIN = 11;

// Setup a oneWire instance to communicate with any OneWire devices 
// (not just Maxim/Dallas temperature ICs)
OneWire oneWire(DS18B20_PIN);
 
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensorTemperature(&oneWire);

const char DATA_LOGGER_FILE[9] = {'e','m','l','o','g','.','c', 's', 'v'};

const byte FUNC_TEMPERATURE = 0;
const byte FUNC_PH = 1;
const byte FUNC_CONDUCTIVITY = 2;

float sensorValues[] = {0.0f, 0.0f, 0.0f};
const int SENSOR_READ_FREQ[] = {100, 100, 100};
String sensorLabels[] = {"Temp.", "pH", "Conduct."};
volatile boolean sensorEnabled[] = {false, false, false};
const int nbSensors = sizeof(sensorValues)/sizeof(sensorValues[0]);
float sensorValuesToRecord[nbSensors];
volatile boolean recordSd = false;

boolean sdError = false;
long sensorReadTime[3] = {0, 0, 0};

// Conductivity sensor
// *** EC Calibration parameters
const float CONDUCT_A = 0.0003;
const float CONDUCT_B = -2.0634;
const float CONDUCT_C = 4129.1;
// *** Parameters for temperature correction
const float CONDUCT_D = -0.0287;
const float CONDUCT_E = 1.5757;
// *** Resistance between P1 and P3
//const float CONDUCT_R1 = 1000;  // ohm
const float CONDUCT_R1 = 470;  // ohm
// Conductivity at 20°
float CONDUCT_CS = 0;
// *** Water temperature for correction
float CONDUCT_TEMPER_C = 25; // TODO: read it from the sensor?
// *** Power in V
const float VCC = 5.;
float ANALOG_TO_VOLT_CONV = VCC/1024.;
const int CONDUCT_READ_DELAY = 50;
const int CONDUCT_READ_NB = 10; // numero di letture con cui fare la media

void setup() {
  Serial.begin(9600);
  
  pinMode(BUZZER_PIN, OUTPUT);

  pinMode(CONDUCT_P1_PIN, OUTPUT);
  pinMode(CONDUCT_P3_PIN, OUTPUT);  

  // Initialize the serial port, wire library and RTC module
  Wire.begin();
  RTC.begin();
  //If we remove the comment from the following line, we will set up the module time and date with the computer one
  RTC.adjust(DateTime(__DATE__, __TIME__));
  
  sensorTemperature.begin();

  lcd.begin(20, 4); // initialize the lcd for 20 chars 4 lines and turn on backlight
  // ------- Quick 3 blinks of backlight  -------------
  for (int i=0; i<3; i++) {
    lcd.backlight();
    delay(250);
    lcd.noBacklight();
    delay(250);
  }
  lcd.backlight();
  
  // NOTE: Cursor Position: CHAR, LINE) start at 0  
  display(3, 0, "OpenLab Matera");
  display(0, 1, "Misuratore portatile");
  display(4, 2, "Inquinamento");  
  displayDateHour(0, 3);
  delay(4000);
  lcd.clear();
  display(0, 0, "Init. SD card...");

  // See if the card is present and can be initialized:
  pinMode(SS_PIN_A, OUTPUT); 
  if (!SD.begin(SS_PIN_A)) {
    beep(3);
    display(0, 1, "Card not found");
    sdError = true;
    delay(4000);
  }
  else {
    display(0, 1, "Card initialized!");
    delay(3000);
  }
  lcd.clear();
  displayMainScreen();
  
  attachInterrupt(5, recordBtnPressed, RISING);
}

void loop() {
  sensorEnabled[FUNC_TEMPERATURE] = digitalRead(ITR_TEMPER_PIN);
  sensorEnabled[FUNC_PH] = digitalRead(ITR_PH_PIN);
  sensorEnabled[FUNC_CONDUCTIVITY] = digitalRead(ITR_CONDUCT_PIN);

  if (recordSd && !sdError && hasSensorEnabled()) {
    writeSensorValuesSd();
  }
  else {
    for (int func=0; func<nbSensors; func++) {
      if (sensorEnabled[func]) {
        if (millis() - sensorReadTime[func] >= SENSOR_READ_FREQ[func]) {
          sensorValues[func] = readSensor(func);
          sensorReadTime[func] = millis();
        }
      }
    }
  }

  displayMainScreen();
}

// Interrupt
void temperBtnPressed() {
  sensorEnabled[FUNC_TEMPERATURE] = digitalRead(ITR_TEMPER_PIN);
}

// Interrupt
void phBtnPressed() {
  sensorEnabled[FUNC_PH] = digitalRead(ITR_PH_PIN);
}

// Interrupt
void conductBtnPressed() {
  sensorEnabled[FUNC_CONDUCTIVITY] = digitalRead(ITR_CONDUCT_PIN);
}

// Interrupt
void recordBtnPressed() {
  recordSd = true;
}

void beep() {
   for (int i=0; i<200; i++) {
     digitalWrite(BUZZER_PIN, HIGH);
     delay(1);
     digitalWrite(BUZZER_PIN, LOW);
     delay(1);
   }  
}

void beep(int nbBeep) {
  beep();
  for (int i=0; i<nbBeep-1; i++) {
    delay(200);
    beep();
  }
}

void displayMainScreen() {
  display(0, 0, "Temp.:");
  display(7, 0, sensorValues[FUNC_TEMPERATURE]);
  display(16, 0, sensorEnabled[FUNC_TEMPERATURE]);
  display(0, 1, "pH:");
  display(7, 1, sensorValues[FUNC_PH]);
  display(16, 1, sensorEnabled[FUNC_PH]);  
  display(0, 2, "Cond.:");
  display(7, 2, sensorValues[FUNC_CONDUCTIVITY]);
  display(16, 2, sensorEnabled[FUNC_CONDUCTIVITY]);  
}

float readSensor(byte curFunction) {
  switch(curFunction) {
    case FUNC_TEMPERATURE:
      sensorTemperature.requestTemperatures();
      return sensorTemperature.getTempCByIndex(0);
    case FUNC_PH: // TODO
      return 99.99;
    case FUNC_CONDUCTIVITY:
      return readConductivity();
  }
}

float readConductivity() {
  float conductFact = 0.;
  float conductAvg1 = 0.;
  float conductAvg2 = 0.;
  float conductAvg3 = 0.;
  float conductV1 = 0.;
  float conductV2 = 0.;
  float conductVSG = 0.;
  float conductRs1 = 0.;
  float conductRs2 = 0.;
  // *** Resistance between the two electrodes
  float conductRs = 0;
  // *** Resistance corrected to bring it to 20°
  float conductRsCor = 0.;

  // Loop to read conductivity
  for (int i=0; i<CONDUCT_READ_NB; i++) {
    // Read it one way
    digitalWrite(CONDUCT_P1_PIN, HIGH);
    digitalWrite(CONDUCT_P3_PIN, LOW);
    delay(CONDUCT_READ_DELAY);
    conductAvg1 = conductAvg1 + ANALOG_TO_VOLT_CONV * analogRead(CONDUCT_P2_PIN);
    // Read it with no voltage
    digitalWrite(CONDUCT_P1_PIN, LOW);
    delay(CONDUCT_READ_DELAY);
    conductAvg2 = conductAvg2 + ANALOG_TO_VOLT_CONV * analogRead(CONDUCT_P2_PIN);
    // Read it the other way
    digitalWrite(CONDUCT_P3_PIN, HIGH);
    delay(CONDUCT_READ_DELAY);
    conductAvg3 = conductAvg3 + ANALOG_TO_VOLT_CONV * analogRead(CONDUCT_P2_PIN);
  }
  // Set no voltage
  digitalWrite(CONDUCT_P1_PIN, LOW);
  digitalWrite(CONDUCT_P3_PIN, LOW);
  // Compute the averages
  conductVSG = conductAvg2 / CONDUCT_READ_NB;
  conductV1  = conductAvg1 / CONDUCT_READ_NB;
  conductV2  = conductAvg3 / CONDUCT_READ_NB;
  // Galvanic correction (TODO, change it?)
  conductV1 = conductV1-conductVSG;
  conductV2 = conductV2-conductVSG;
  // First computing of RS
  if (abs(conductV1) < 0.004) {
    conductRs1 = 60000.;
  }
  else {
    conductRs1 = CONDUCT_R1*(VCC - conductV1) / conductV1; 
  }
  // Second computing of RS and average (RS in kohm)
  if (abs((VCC - conductV2)) < 0.004) {
    conductRs2 = 60000.;
  }
  else {
    conductRs2 = CONDUCT_R1 * conductV2 / (VCC - conductV2);
  }
  conductRs = (conductRs1 + conductRs2) / 2;
  // Compute temperature factor
  conductFact = CONDUCT_D * CONDUCT_TEMPER_C + CONDUCT_E; // TODO: read it from the sensor?
  if (conductFact < 0) {
    conductFact = - conductFact;
  }
  if(abs(conductFact) < 0.01){
    conductFact = 1;
    display(7, 2, "Error conduct.");
  }
  // Resistance correction to bring it to 20°
  conductRsCor = conductRs / conductFact;
  // CS
  int cs = CONDUCT_A * pow(conductRsCor, 2) + CONDUCT_B * conductRsCor + CONDUCT_C;
  return cs;  
}

void writeSensorValuesSd() {
  DateTime now = RTC.now();
  File dataFile = SD.open(DATA_LOGGER_FILE, FILE_WRITE);
  String dataString = String(now.day()) + "/" + String(now.month()) + "/" + String(now.year()) + ";";
  dataString += String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second()) + String(";");
  for (int i=0; i<nbSensors; i++) {
    if (sensorEnabled[i]) {
      dataString += String(sensorValuesToRecord[i]);
    }
    dataString += ";";
  }
  
  // if the file is available, write to it:
  if (dataFile) {
    beep();
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    beep(3);
    lcd.clear();
    display(0, 0, "Error opening file");
    display(0, 1, DATA_LOGGER_FILE);
    display(0, 2, "No record writen");
    sdError = true;
    recordSd = false;
    delay(4000);
    lcd.clear();
  }
  recordSd = false;
}

boolean hasSensorEnabled() {
  for (int i=0; i<nbSensors; i++) {
    if (sensorEnabled[i]) {
      return true;
    }
  }
  return false;
}

void displayDateHour(byte col, byte line) {
  DateTime now = RTC.now();
  displayOn2Digits(col, line, now.day());
  display(col+2, line, "/");
  displayOn2Digits(col+3, line, now.month());
  lcd.setCursor(col+5, line);
  lcd.print('/');
  lcd.setCursor(col+6, line);
  lcd.print(now.year());
  displayOn2Digits(col+11, line, now.hour());
  lcd.setCursor(col+13, line);
  lcd.print(':');
  displayOn2Digits(col+14, line, now.minute());
  lcd.setCursor(col+16, line);  
  lcd.print(':');
  displayOn2Digits(col+17, line, now.second());
}

void displayOn2Digits(byte col, byte line, unsigned char val) {
  lcd.setCursor(col, line);
  if (int(val) < 9) {
    lcd.print('0');
    lcd.setCursor(col+1, line);
  }
  lcd.print(val);
}

void display(byte col, byte line, unsigned char val) {
  lcd.setCursor(col, line);
  lcd.print(val);
}

void display(byte col, byte line, float val) {
  lcd.setCursor(col, line);
  lcd.print(val);
}

void display(byte col, byte line, float val, byte nbDec) {
  lcd.setCursor(col, line);
  lcd.print(val, nbDec);
}

void display(byte col, byte line, int val) {
  lcd.setCursor(col, line);
  lcd.print(val);
}

void display(byte col, byte line, String text) {
  lcd.setCursor(col, line);
  lcd.print(text);
}
