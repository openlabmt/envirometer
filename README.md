Envirometer
===========
Portable system to detect polluted water.

It has sensors to measure water conductibility, temperature and pH.
It is based on an ArduinoMega board with LCD display.

See the [wiki](http://wiki.openlabmatera.org/doku.php/progetti/envirometer/start) for more information.
